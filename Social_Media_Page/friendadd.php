<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>Friend Add</title>
</head>
<body>
<?php

session_start();
if(isset($_SESSION["user"]))
{
	echo "<p>",$_SESSION["user"], "'s Add Friend Page</p>";

	
}

$showNextButton = true;
$showPrevButton = false;


	require_once ("settings.php"); //connection info
	$conn = @mysqli_connect($host,
		$user,
		$pwd,
		$sql_db
	
	);
	// Checks if connection is successful
	if (!$conn) {
		//Displays error Message
		echo "<p>Database Connection failure</p>";
	
	} else  //successful connection
	{
		$friendIDs = array();

		//initial query to get the friend_ID of the user by using the session email of the user who is logged in.
		$getIdQuery = "SELECT friend_email, friend_id FROM friends";
		$idResult = mysqli_query($conn, $getIdQuery);
		while ($row = mysqli_fetch_assoc($idResult))
		{
			if($row["friend_email"] === $_SESSION["email"]) 
			{
				$userID = $row["friend_id"];	
			}
		}
		mysqli_free_result($idResult);
		$myfriendQuery = "SELECT friend_id1, friend_id2 FROM myfriends";
		$myfriendResult = mysqli_query($conn, $myfriendQuery);
		if(!$myfriendResult)
		{
			echo "<p>Something is wrong with ", $myfriendQuery, "</p>";
		}
		else 
		{
			//
			//This loop checks for the users friend_id in each of the rows of the myfriend table
			//if the user is found then we add the OTHER friend_ID to a list
			while ($myfriendrow = mysqli_fetch_assoc($myfriendResult)) 
			{
				if($myfriendrow["friend_id1"] === $userID)
				{
					if(!in_array($myfriendrow["friend_id2"],$friendIDs))
					{
						array_push($friendIDs,$myfriendrow["friend_id2"]);
					}
				}
				if($myfriendrow["friend_id2"] === $userID)
				{
					if(!in_array($myfriendrow["friend_id1"],$friendIDs))
					{
						array_push($friendIDs,$myfriendrow["friend_id1"]);	
					}
				}
			}
		mysqli_free_result($myfriendResult);
		//this check makes sure that the session variable matches 
		//the table data. will handle if a user has been unfriended.
		if(isset($_SESSION["num_friends"]))
		{
			if($_SESSION["num_friends"] != count($friendIDs))
			{
				$_SESSION["num_friends"] = count($friendIDs);
			}
		}
		if(isset($_SESSION["num_friends"]))
		{
			echo "<p> Total Number of friends is ",$_SESSION["num_friends"]," </p>";
		}
			//this query will retrieve the informatation of all the friends from the friends table.
			$friendQuery = "SELECT profile_name, friend_email, friend_id FROM friends ORDER BY profile_name";
			$friendResult = mysqli_query($conn, $friendQuery);
			$notfriends = array();	
			$notfriendIDs = array();
			while ($row = mysqli_fetch_assoc($friendResult)) 
			{
				//find the row which contains current user email and
				//dont display it.	
				if($row["friend_email"] !== $_SESSION["email"]) 
				{
					//loop over all rows and find any row which contains
					//and id which is NOT in our friends array.
					for($i = 0; $i < count($row); $i++)
					{
						if(!in_array($row["friend_id"], $friendIDs))
						{
							if(!in_array($row["profile_name"],$notfriends))
							{
								//if row is NOT a friend of the current user. 
								//and is NOT already in the notfriends array.
								//add to notfreinds array.
								array_push($notfriends,$row["profile_name"]);
								array_push($notfriendIDs,$row["friend_id"]);
							}
						}
					}	
				}	
			}
			if(isset($_SESSION["pageIndex"]))
			{
				$pageIndex = $_SESSION["pageIndex"];
			}
			else
			{
				$pageIndex = 0;
			}
			//display all results which are not friends with the current user.
			for($i = $pageIndex; $i < $pageIndex + 5; $i++)
			{
				if(count($notfriends) > $pageIndex)
				{
					if($i < count($notfriends))
					{
						echo "<table width = \"400px\" border = \"1\">";
						echo "<tr>\n";
						echo "<td width=\"100\">",$notfriends[$i],"</td>\n";
						//echo the add friend button. this sets the post value which can then call the Addfriend() function
						echo "<td align = \"center\" width=\"50\"><form method = \"post\" action=\"friendadd.php\">
							<button type = \"submit\" name = \"addfriend\" value = \"$notfriendIDs[$i]\">Add as friend</button>
							</form></td>\n";

					}
					if($i < count($notfriends) - 1)
					{
						
					$showNextButton = true; 
					}
					else
					{
						$showNextButton = false; 
					}
				}
			}
		echo "</table>\n";			
		}
		
		
		//check if add friend button has been pressed
		//if so: pass through the connection information.
		// and the related friend ID.
		if(isset($_POST["addfriend"]))
		{
			$friendToAdd =	$_POST["addfriend"];	
			AddFriend($conn,$friendToAdd);
			header("Location: friendadd.php");
		}
		mysqli_free_result($friendResult);
		CheckFriends($conn,$userID);
		mysqli_close($conn);
		
		}
		// if no friends to add.. dont show buttons.
		if(count($notfriends) > 0)
		{
			//if your not on the first page show previous button.
			if($pageIndex >= 5)
			{
				$showPrevButton = true;
			}
			else
			{
				$showPrevButton = false;
			}
			
			
			echo "<div id = \"prvbtn\">";
			if($showPrevButton)
			{
			echo "<form method = \"post\" action=\"friendadd.php\">
				<button type = \"submit\" name = \"pagedown\" value = \"$pageIndex\">Previous</button>
				</form></td>\n";
			}
			echo "</div>";
			echo "<div id = \"pgno\">";
			// show page number. probably not the most efficient way
			// to do this but it works.
			echo "Page ", ($pageIndex + 5) /5;
			
			echo "</div>";
		
			echo "<div id = \"nxtbtn\">";
			if($showNextButton)
			{
			echo "<form method = \"post\" action=\"friendadd.php\">
				<button type = \"submit\" name = \"pageup\" value = \"$pageIndex\">Next</button>
				</form></td>\n";
			}
		}
	echo "</div>";
	if(isset($_POST["pageup"]))
	{
		PageUp($_POST["pageup"]);
	}
	if(isset($_POST["pagedown"]))
	{
		PageDown($_POST["pagedown"]);
	}	
	
	function PageUp($pageIndex)
	{
		$pageIndex += 5;
		$_SESSION["pageIndex"] = $pageIndex;
		header("Location: friendadd.php");
	}
	function PageDown($pageIndex)
	{
		$pageIndex -= 5;
		$_SESSION["pageIndex"] = $pageIndex;
		header("Location: friendadd.php");
	}
	function AddFriend($conn, $val)
	{
		$duplicateFound = false;
		$getIdQuery = "SELECT friend_email, friend_id FROM friends";
		$idResult = mysqli_query($conn, $getIdQuery);
		while ($row = mysqli_fetch_assoc($idResult))
		{
			if($row["friend_email"] === $_SESSION["email"]) 
			{
				$userID = $row["friend_id"];	
			}
		}
		$checkDuplicateQuery = "SELECT friend_id1,friend_id2 FROM myfriends";
		$checkDuplicateResult = mysqli_query($conn, $checkDuplicateQuery);
		while ($row = mysqli_fetch_assoc($checkDuplicateResult))
		{	
			if($row["friend_id1"] == $userID && $row["friend_id2"] == $val)
			{
				$duplicateFound = true;
			} 
			if($row["friend_id2"] == $userID && $row["friend_id1"] == $val)
			{
				$duplicateFound = true;
			} 
		}
		
		if(!$duplicateFound)
		{
		mysqli_free_result($idResult);
			$addfriendQuery = "INSERT INTO myfriends (friend_id1,friend_id2)
								VALUES($userID,$val)
								 ON DUPLICATE KEY UPDATE friend_id1 =$userID,
								 friend_id2 =$val";
			
			$result = mysqli_query($conn, $addfriendQuery);
			$num_friends = $_SESSION["num_friends"];
			
			$numfriendquery = "UPDATE friends SET num_of_friends = $num_friends WHERE friend_id = $userID";
			$numfriendResult = mysqli_query($conn, $numfriendquery);

		}
			#$_SESSION["num_friends"] = $num_friends;
		}
	function CheckFriends($conn, $userID)
	{

		$numFriends = 0;
		$myfriendQuery = "SELECT friend_id1, friend_id2 FROM myfriends";
		$myfriendResult = mysqli_query($conn, $myfriendQuery);
		if(!$myfriendResult)
		{
			echo "<p>Something is wrong with ", $myfriendQuery, "</p>";
		}
		else 
		{
			while ($myfriendrow = mysqli_fetch_assoc($myfriendResult)) 
			{
				if($myfriendrow["friend_id1"] === $userID)
				{
					$numFriends++;
				}
				if($myfriendrow["friend_id2"] === $userID)
				{
					$numFriends++;
				}
			}
		mysqli_free_result($myfriendResult);
		
	}
	
	}
?>

<p><a href ="friendlist.php">Friend Lists</a>
<a href ="logout.php">Log Out</a></p>
</body>
</html> 