<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>Registration</title>
  </head>
  <body>
   	<h1>Friend System</h1>	
   	<h2>Registration Page</h2>
	
	<div id = "needsBorder">
	<div id= "idform">
		
	<form id = "friend" method = "post" action = "signup.php" novalidate>	
	
			<label for="email">Email Address:</label> 
			<input type="text" name= "email" id="email" maxlength="20" size="30"  /> 
	
			<label for="profile_name">Profile Name:</label> 
			<input type="text" name= "profile_name" id="profile_name" maxlength="30" size="25"/>
		
		
		<label for="password">Password</label> 
			<input type="password" name= "password" id="password" maxlength="20" size="20"  />
			<label for="confirm_password">Confirm Password</label> 
			<input type="password" name= "confirm_password" id="confirm_password" maxlength="20" size="20"  /> 
	<br/>
	</div>
	
	<div id = "bottom">
	<input type= "submit" name= "submit" value="Register"/>			
	<button type= "reset">Clear</button>						
	</div>
	</form>
	</div>
	
	
	
<?php

require_once ("settings.php"); //connection info

	// Check form has been submitted
if(isset($_POST["submit"]))
{
	session_start();
	$conn = @mysqli_connect($host,$user,$pwd,$sql_db);
	
	if (!$conn) 
	{
		echo "<p>Database Connection failure</p>"; //Connection did not work.

	} 
	else 
	{

		// validate		
		//Connection successful
		$errmsg = "";	
		
		//Validate Email to match email format
		if(empty($_POST["email"]))
		{
			$errmsg .= "<p>Please Enter an Email Address!</p>";	
		}
		else
		{
			if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) 
			{

				$email = $_POST["email"];
				$query = "SELECT friend_email FROM friends";
				$result = mysqli_query($conn,$query);
				if($result){

				while ($row = mysqli_fetch_assoc($result)) 
				{
					if($email === $row["friend_email"])
					{	

						$errmsg .= "<p>That email address is already registered!</p>"; 
					}
				}
				}
			} 
			else
			{
				$errmsg .= "<p>Email not a valid email address!</p>";
			}
			
		}
		
		//Validate profile name Only letters
		if(empty($_POST["profile_name"]))
		{  
			$errmsg .= "<p>Please Enter a Profile name!</p>";
		} 
		else
		{
			
			if (ctype_alnum(str_replace(' ','',$_POST["profile_name"]))) {
				$profile_name = $_POST["profile_name"];
			}else
			{
				$errmsg .= "<p>Profile name may only be letters and numbers!</p>";
			}		
		}
		
		
		//Validate password for only letters and numbers
		if(empty($_POST["password"]))
		{
			$errmsg .= "<p>Please Enter a password!</p>";
		} else 
		{
			if(ctype_alnum($_POST["password"]))
			{
				$password = $_POST["password"];	
			} else {
				$errmsg .= "<p>Password may only contain Letters and Nubmers!</p>";
			}
		}
		
		//Validate confirm password to match password
		if(empty($_POST["confirm_password"]))
		{
			$errmsg .= "<p>Please Enter a confirmation password!</p>";
		} else 
		{
			if(isset($password))
			{
				if($_POST["confirm_password"] != $password){
					$errmsg .= "<p>Passwords must match!</p>";
					
				}
			}
		}
			
		if($errmsg != "")
		{
			echo "<div id=\"errorMsg\">". $errmsg ."</div>";	

		}
		else
		{
			$date = date('y/m/d');
			$query = "INSERT INTO friends(friend_id,friend_email,password,profile_name,date_started,num_of_friends)
					VALUES (
					'NULL','$email', '$password', '$profile_name', '$date', '0'  
					);";
		
			$result = mysqli_query($conn,$query);
			if($result)
			{ 
				header("Location: friendadd.php");
				$_SESSION["email"] = $email;
				$_SESSION["user"] = $profile_name;
				$_SESSION["num_friends"] = 0;
				}
				else
				{
					echo"<p> Error within Query</p>";
				}
			}
		}
	}
function sanitise_input($data) 
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
	
?>  
</body>
<footer>
  <p><a href ="index.php">Home</a></p>
</footer>
</html> 