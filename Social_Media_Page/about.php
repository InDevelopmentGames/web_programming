<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>DevSpace</title>
</head>
<body>
<div id = "needsBorder">

<h1>About Page</h1>
<ul>
	<li>For this assignment I have attempted to complete all the requirements
	of <strong>Tasks 1->8 </strong> </li>
	<li>Troubles:</li>
	<ol>
		<li>I had minor troubles figuring out how to correctly add recoreds to a database from a text file</li>
		<li>I also had troubles implementing the add and remove friend functionality</li>
		<li>An error began appearing when trying to call header() via a form post. This issue stopped occuring
		through my iterations, however I never figured out why it stopped.</li>
		<li>Despite any troubles I had during the process, I managed to work through each one</li>
	</ol>
	<li>Next time i would like to implement better usage of CSS</li>
 <li>Extra Features:</li>
	<ol>
		<li>Friends Lists will automatically update for each user if they have been unfriended.</li>
		<li>Added Pagination functionality to <strong>friendadd.php.</strong></li>
	</ol>
	<li><a href="friendlist.php"> Link to Friend List Page</a></li>
	<li><a href="friendadd.php"> Link to Add Friend Page</a></li>
	<li><a href="index.php"> Link to Index Page</a></li>
</ul>
</div>
<p>
<a href ="index.php">Return Home</a>
</p>
</body>
</html>