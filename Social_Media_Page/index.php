<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>Home</title>
</head>
<body>
<div id="needsBorder"> 
	<h1>DevSpaceBook</h1>
	<p> Name: Nathanial Preller </p>
	<p>Student Number : 101007303 </p>
	<p> Email Address : <a href="mailto:s101007303@student.swin.edu.au">s101007303@student.swin.edu.au</a> </p>
	<p>I declare that this assignment is my individual work. I have not worked collaboratively nor have I copied from any other students work or from any other source </p>

 <?php 
if(isset($_SESSION["pageIndex"]))
	{
		$_SESSION["pageIndex"] = 0;
	}
 $submit = true;
 $errmsg = "";
 require_once ("settings.php"); //connection info
	$conn = @mysqli_connect($host,
		$user,
		$pwd,
		$sql_db
	);
	if (!$conn) {
		echo "<p>Database Connection failure</p>"; //Connection did not work.
	} 
	else {
			
	// validate		
	
	
	}	
	
	// create friends table
	//**********//
	if(empty($result))
	{
		$query = "CREATE TABLE IF NOT EXISTS friends 
		(
			friend_id INT NOT NULL AUTO_INCREMENT,
			PRIMARY KEY(friend_id),
			friend_email VARCHAR(50) NOT NULL,
			password VARCHAR(20) NOT NULL,
			profile_name VARCHAR(30) NOT NULL,
			date_started DATE NOT NULL,
			num_of_friends INT UNSIGNED 
			);";
		 
		$result = mysqli_query($conn, $query);
	}
	// Insert into friends table 
	//**********//
	//Ensure there will be no duplicates when loading from text file.

	$results = mysqli_query($conn, "SELECT * FROM friends");
	if ($results) 
	{ 
		if($results->num_rows === 0)
		{
			$query = "LOAD DATA LOCAL INFILE  'samplefriends.txt' INTO TABLE friends
						FIELDS TERMINATED BY ','   
						LINES TERMINATED BY '\n'
						;";
				
			$result = mysqli_query($conn, $query);	
			if(!$result)
			{
				$submit = false;	
				$errmsg .= "<p>Error Poplulating the <strong>friends</strong> table from txt file.</p>";
			}
			else
			{
			}
		}
	}
	// Create myfriends table
	//**********//
	$query = "CREATE TABLE IF NOT EXISTS myfriends(
				friend_id1 INT NOT NULL,
				friend_id2 INT NOT NULL
	);";
	
	$result = mysqli_query($conn, $query);
	
		if(!$result)
		{
			$submit = false;
			$errmsg .= "<p>Error Creating the <strong>myfriends</strong> table</p>";
		}
		
		
	///
	//Ensure there will be no duplicate when loading from text file.
	///
	$results = mysqli_query($conn, "SELECT * FROM myfriends");
	if ($results)
	{ 

		if($results->num_rows === 0)
		{
			//table is empty. so load in the data.
			$query = "LOAD DATA LOCAL INFILE  'samplemyfriends.txt' INTO TABLE myfriends
							FIELDS TERMINATED BY ','  
							LINES TERMINATED BY '\n'
							;";
					
			$result = mysqli_query($conn, $query);	
		
			if(!$result)
			{
				$submit = false;	
				$errmsg .= "<p>Error Poplulating the <strong>myfriends</strong> table from txt file.</p>";
			}
			else
			{
				
			}
		}
	}
	// if everything is a success than submit! 
	//**********//
		if(!$submit)
		{
			//echo $_SESSION["login"];
			echo "<p id = \"err\">", $errmsg, "</p>";			
		}else{
			echo "<p>Tables Created and populated successfully!</p>";
		}

	
 ?>
 
  <p><a href = "signup.php">Sign up</a>
 <a href = "login.php">Login</a>
 <a href = "about.php">About</a></p>
  </div>
</body>
</html>
	