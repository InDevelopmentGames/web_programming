<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>Friend List</title>
</head>
<body>
<?php
	require_once ("settings.php"); //connection info
	$conn = @mysqli_connect($host,
		$user,
		$pwd,
		$sql_db
	
	);
session_start();
if(isset($_SESSION["user"]))
{
	echo "<p>",$_SESSION["user"], "'s Friend List Page</p>";

	
}
if(isset($_SESSION["pageIndex"]))
	{
		$_SESSION["pageIndex"] = 0;
	}

	// Checks if connection is successful
	if (!$conn) {
		//Displays error Message
		echo "<p>Database Connection failure</p>";
	
	} else {
		//Upon successful connection
		$sql_table="friends";
		$friendIDs = array();
		$getIdQuery = "SELECT friend_email, friend_id FROM friends";
		$idResult = mysqli_query($conn, $getIdQuery);
		while ($row = mysqli_fetch_assoc($idResult))
		{
			if($row["friend_email"] === $_SESSION["email"]) 
			{
				$userID = $row["friend_id"];	
			}
		}
		
		//Setup the SQL command to add the data to the table
		$myfriendQuery = "SELECT friend_id1, friend_id2 FROM myfriends";
		$myfriendResult = mysqli_query($conn, $myfriendQuery);
		if(!$myfriendResult)
		{
			echo "<p>Something is wrong with ", $myfriendQuery, "</p>";
		}
		else 
		{
			while ($myfriendrow = mysqli_fetch_assoc($myfriendResult)) 
			{
				if($myfriendrow["friend_id1"] === $userID)
				{
					if(!in_array($myfriendrow["friend_id2"],$friendIDs))
					{
						array_push($friendIDs,$myfriendrow["friend_id2"]);
					}
				}
				if($myfriendrow["friend_id2"] === $userID)
				{
					if(!in_array($myfriendrow["friend_id1"],$friendIDs))
					{
						array_push($friendIDs,$myfriendrow["friend_id1"]);	
					}
				}
			}
			mysqli_free_result($myfriendResult);
			$friendQuery = "SELECT profile_name, friend_email, friend_id FROM friends ORDER BY profile_name";
			$friendResult = mysqli_query($conn, $friendQuery);
			$friends = array();	
			//Loop to find all friends
			while ($row = mysqli_fetch_assoc($friendResult)) 
			{
				for($i = 0; $i < count($friendIDs); $i++)
				{
					if($row["friend_email"] !== $_SESSION["email"]) 
					{
						if($row["friend_id"] === $friendIDs[$i])
						{
							if(!in_array($row["profile_name"],$friends))
							{
								//add to freinds array.
								array_push($friends,$row["profile_name"]);
							}
						}
					}	
				}
			}
			//update the session friend count.
			if(isset($_SESSION["num_friends"]))
			{
				if($_SESSION["num_friends"] != count($friendIDs))
				{
					$_SESSION["num_friends"] = count($friendIDs);
				}
			echo "<p> Total Number of friends is ",$_SESSION["num_friends"]," </p>";	
			}

		for($i = 0; $i < count($friends); $i++)
			{
				echo "<table width = \"400px\" border = \"1\">";
				echo "<tr>\n";
				echo "<td width=\"100\">",$friends[$i],"</td>\n";
				echo "<td align = \"center\" width=\"50\"><form method = \"post\" action=\"friendlist.php\">
					<button type = \"submit\" name = \"unfriend\" value = \"$friendIDs[$i]\"> Unfriend</button>
					</form></td>\n";
				echo "</table>\n";
			}
		}
		
		if(isset($_POST["unfriend"]))
			{
				$friendToRemove =$_POST["unfriend"];	
				Unfriend($conn,$friendToRemove);
				//exit();
				header("Location: friendlist.php");
			}
		mysqli_close($conn);
	}	
	
	function Unfriend($conn, $val)
	{
		$friendFound = false;
		$getIdQuery = "SELECT friend_email, friend_id FROM friends";
		$idResult = mysqli_query($conn, $getIdQuery);
		while ($row = mysqli_fetch_assoc($idResult))
		{
			if($row["friend_email"] === $_SESSION["email"]) 
			{
				$userID = $row["friend_id"];	
			}
		}
		$checkDuplicateQuery = "SELECT friend_id1,friend_id2 FROM myfriends";
		$checkDuplicateResult = mysqli_query($conn, $checkDuplicateQuery);
		//
		//This loop checks for the users friend_id in each of the rows of the myfriend table
		//if the user is found then we add the OTHER friend_ID to a list
		while ($row = mysqli_fetch_assoc($checkDuplicateResult))
		{	
			if($row["friend_id1"] == $userID && $row["friend_id2"] == $val)
			{
				$friend_id1 = $row["friend_id1"];
				$friend_id2 = $row["friend_id2"];
				$friendFound = true;
			} 
			if($row["friend_id2"] == $userID && $row["friend_id1"] == $val)
			{
			
				$friend_id1 = $row["friend_id1"];
				$friend_id2 = $row["friend_id2"];
				$friendFound = true;
			} 
		}
		
		if($friendFound)
		{
			mysqli_free_result($idResult);
				$addfriendQuery = "DELETE FROM myfriends 
								WHERE friend_id1 = $friend_id1
								AND	friend_id2 = $friend_id2";
			
			$result = mysqli_query($conn, $addfriendQuery);
	
		$num_friends = $_SESSION["num_friends"];

		$numfriendquery = "UPDATE friends SET num_of_friends = $num_friends WHERE friend_id = $userID";
		$numfriendResult = mysqli_query($conn, $numfriendquery);

		}
	}
?>

<p><a href ="friendadd.php">Add Friends</a>
<a href ="logout.php">Log Out</a></p>
</body>
</html> 