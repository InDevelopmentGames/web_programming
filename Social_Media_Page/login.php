<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="styles/mystyle.css">
  <title>Login</title>
</head>
<body>
   	<h1>DevSpaceBook</h1>	
   	<h2>Login Page</h2>	
	<div id = "needsBorder">
	<form id = "friend" method = "post" action = "login.php" novalidate>	
			<label>Email Address:</label> 
			<input type="text" name= "email" id="email" maxlength="20" size="30" value = "" placeholder=" " pattern=""  /> <!-- pattern here -->
		</br>
			<label>Password</label> 
			<input type="password" name= "password" id="password" maxlength="20" size="20" placeholder=" " pattern=""  /> <!-- pattern here -->
		</br>
	
	<div id = "bottom">
	<p>
		<input type= "submit" name= "submit" value="Login"/>			
		<button type= "reset">Clear</button>		
	</p>
	</div>
	</form>
	</div>
	
	<?php 
	require_once("settings.php");
	if(isset($_POST["submit"]))
	{
	session_start();
	$conn = @mysqli_connect($host,$user,$pwd,$sql_db);
	
	if (!$conn) 
	{
		echo "<p>Database Connection failure</p>"; //Connection did not work.

	} 
	else 
	{	
		$errmsg = "";		
		$passed = false;
		$wrongpw = false;
		$wrongEmail = false;
		if(empty($_POST["email"]))
		{
			$errmsg .= "<p>Please Enter an Email Address</p>";	
		}
		else
		{
			if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) 
			{
				$email = $_POST["email"];
				$password = $_POST["password"];
				$profile_name = "";
				$query = "SELECT friend_email,password,profile_name,num_of_friends FROM friends";
				$result = mysqli_query($conn,$query);
				if($result)
				{  
					while ($row = mysqli_fetch_assoc($result)) 
					{
						if($email === $row["friend_email"])
						{	
							if($password === $row["password"])
							{
							$passed = true;

							}
							else
							{
							$wrongpw = true;	
							$passed = false;
							}
						$wrongEmail = false;
						$profile_name = $row["profile_name"];
						$num_friends = $row["num_of_friends"];
						$passed = true;
						break;
						}
						else
						{
							$wrongEmail = true;
							$passed = false;
						}
					}
				}
			} 
			else
			{
				$errmsg .= "<p>email not a valid email address</p>";
			}
			
		}
		
		if($wrongEmail)
		{
		$errmsg .= "<p>email does not exist</p>";
	
		}
		if($wrongpw){
		$errmsg .= "<p>incorrect password</p>";
			
		}
		if($errmsg != "")
		{	
			echo "<div id=\"errorMsg\">". $errmsg ."</div>";
		}
		else
		{
			if($passed)
			{
				$_SESSION["email"] = $email;
				$_SESSION["user"] = $profile_name;
				$_SESSION["num_friends"] = $num_friends;
				header("Location: friendlist.php");
			}
		
		}
	
	}
}

	?>

</body>
<footer>
 <p><a href = "index.php">Return Home</a>Dont have an account?            <a href = "signup.php"> Create a new one!</a></p>
</footer>
</html>