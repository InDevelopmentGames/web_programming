<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Jobs Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 <link href= "styles/style.css" rel="stylesheet"/>
 </head>
 
 
 <body>
 <div id = "needsBorder">
 <?php
$errmsg = "";
$matches = array();
	if(empty($_GET["jobtitle"]))
	{
		$errmsg .= "Please Enter Job Title to search.<br>";
	} else
	{
		$job_title = $_GET["jobtitle"];
		$filename = "../../data/assign1/jobs.txt";
	}
		if (!file_exists($filename) || filesize($filename) == 0) 
		{
			echo "<p>There are no messages posted.</p>";
		}
			else 
			{
				//Search by jobtitle
				//////////////////////////////////////
				$handle = fopen($filename, "r");
				echo "<p><strong> Job Vacany Information" . "</strong><br/>";
				while (! feof($handle) ) 
				{
					$curLine = fgets ($handle);
					$value =  strpos(strtolower($curLine),strtolower($job_title));
					if(strpos(strtolower($curLine),strtolower($job_title)) !== false)
					{			
						$curElement = explode(",", $curLine);
						echo "Title: {$curElement[1]}<br />";
						echo "Description: {$curElement[2]}<br />";
						echo "Closing Date: {$curElement[3]}<br />";
						echo "Position: {$curElement[5]} -  {$curElement[4]} <br />";
						
						echo "Application by: {$curElement[6]}<br />";
						echo "Location: {$curElement[7]}<br />";
						echo "<hr/>";
					}
					else
					{
					echo "<p>No Results found using that query.</p>";
					break;	
					}
	
				}
			fclose($handle);
			}	
			
?>
</div>
<p><a href="index.php"> Return Home</a></p>
</body>
</html>