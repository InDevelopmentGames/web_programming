<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Index Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link href= "styles/style.css" rel="stylesheet"/>
  <title>Faux Game Studios</title>
</head>
<body>
<div id = "needsBorder">

<h1>About Page</h1>
<ul>
<?php
echo "<li>Current PHP version: " . phpversion() . "</li>";

?>
<li>For this assignment I have attempted to complete all the requirements
 of Tasks; 1, 2, 3, 4, 5, 6. </li>
 <li>The tasks I was not able to complete were Tasks 7 and 8 this is due to a family matter that happened during the semester.</li>
 <li>At this point in time there are no special features to this website.</li>

</ul>
</div>
<p><a href="index.php"> Return Home</a></p>
</body>
</html>