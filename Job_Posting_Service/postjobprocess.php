<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Jobs Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 <link href= "styles/style.css" rel="stylesheet"/>
 </head>
 
 
 <body>
 <div id = "needsBorder">
 <?php

$errmsg = "";
$submitform = true;
	
	/// Each Element of the form is checked for validity.
	///the indiviual checks are seperated by slashes like on the line below
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["PositionID"]))
	{
		$errmsg .= "*Position ID field is empty* Please Enter PositionID.<br>";
	} 
	else 
	{ 
		/// Get Position ID from form
		$posID = $_POST["PositionID"];

		///
		/// check if file exists : if it doesnt, there will be no ID Clashes
		/// and we are free to create a new file.
		umask(0007); 
		$dir = "../../data/assign1"; 
		if (!file_exists($dir))
		{
			mkdir($dir, 02770);
		}
		$filename = "../../data/assign1/jobs.txt";
		$handle = fopen($filename,"a");
		$contents = file_get_contents($filename);
		
		///
		///check if Position ID is unique. if it is not, Dont let form be written.
		///
		if(strpos($contents,$posID) !== false)
		{
			$errmsg .= "*Position ID already exists* Please Enter a Unique Position ID.<br/>";
		}
		///
		///Check position id Matches correct format.
		///
		if(!preg_match("/P[0-9]{4}/",$posID ))
		{
			$errmsg .= "*Position id is invalid*  Please enter a ID that matches this format  : [ P1234 ]<br/>"; 			
		}
	}
	///Title
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["Title"]))
	{
		$errmsg .= "*Title field left blank* Please enter a title.<br/>";
	} 
	else{
		$title = $_POST["Title"];

	}
	///Closing Date
	///////////////////////	///////////////////////	///////////////////////
	if (empty($_POST["ClosingDate"]))
	{
		$errmsg .= "*Closing Date field left blank* Please enter a description.<br/>";
	}else
	{
		$date_string = $_POST["ClosingDate"];
		if(!preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{2})/", $date_string))
		{

				$errmsg .= "*Date Contains Invalid Characters*";
		}else
		{
			list($dd,$mm,$yy) = explode('/', $date_string);
			
			if (!checkdate($dd,$mm,$yy)) 
			{

				$errmsg .= "Date Entered is not a valid Date.";
				
			}else{
				
			}
		
		}
	}
	
	///Description
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["Description"]))
	{

		$errmsg .= "*Description field left blank* Please enter a description.<br/>";
	} 
	else{
		$description = $_POST["Description"];

	}	
	///Position
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["Position"]))
	{
		$errmsg .= "*No Position type selected* Please select a Position type.<br/>";
	} 
	else{
		$postype = $_POST["Position"];
	}
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["Contract"]))
	{	
		$errmsg .= "*No Contract type selected* Please select a Contract type.<br/>";
	} 
	else{
		$contract = $_POST["Contract"];

	}
	///Application type
	///////////////////////	///////////////////////	///////////////////////
	if(isset($_POST["Application"]) && is_array($_POST["Application"]))
	{	
		$application = $_POST["Application"];
	
		if(isset($application[1]))
		{
		
		$app = "{$application[0]} \t {$application[1]}"; 
		
		
		}
		else
		{
			$app = "{$application[0]} \t";
		}
		//echo $app;	
	}
	else{
		$errmsg .= "*No Application by selected* Please select a Application by type.<br/>";

	}
	///Location
	///////////////////////	///////////////////////	///////////////////////
	if(empty($_POST["Location"]))
	{
		$errmsg .= "*No Location selected* Please select a Location.<br/>";
	}
	else{
		$location = $_POST["Location"];

	}
	
	///If an error is found then display message.
	///and set a boolean to prevent the contents being written to file
	///////////////////////	///////////////////////	///////////////////////
	if($errmsg != " ")
	{
		$submitform = false;
		echo"<h2>There are Errors with you Application.</h2>";
		echo "<p>",$errmsg,"</p>";
	}
	
	
	//  WRITE CONTENTS TO FILE //
	//////////////////////////////////////////////////////////////////////
	if($submitform)
	{
		echo "Application submitted successfully.";
		
		if($handle)
		{
			$title = "{$posID},\t {$title}, \t{$description},\t{$date_string},\t{$postype},\t{$contract},\t{$app},\t{$location}\n";
			$data = stripslashes($title);
			if(fwrite($handle,$data))
			{
				echo "<p> Thanks For Your Application.</p>";
				fclose($handle);
			}else
			{
				echo "<p>error while writing to file.</p>";
				
			}
		}	
		
	}
	
	
?>
</div>
<p><a href="postjobform.php"> Go Back</a></p>
<p><a href="index.php"> Return Home</a></p>
</body>
</html>