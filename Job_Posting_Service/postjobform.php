<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="Jobs Page" />
  <meta name="keywords" content="HTML5, CSS layout" />
  <meta name="author" content="Nathanial Preller"  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   <link href= "styles/style.css" rel="stylesheet"/>
  <!--<link href= "styles/style.css" rel="stylesheet"/> -->
 </head>
 
 
 <body>
	<form id="apply" method="post" action="postjobprocess.php" novalidate>
 		<h2>Job Vacany Posting System</h2>	
		<p>
			<label for="PositionID">Position ID:</label> 
			<input type="text" name= "PositionID" id="PositionID" maxlength="5" size="6" placeholder=" " pattern="[P]{1}[0-9]{4}"  /> <!-- pattern here -->
		</p>
		<p>
			<label for="Title">Title:</label> 
			<input type="text" name= "Title" id="Title" maxlength="20" size="25" placeholder=" " pattern="^[a-zA-Z .,!]{0,150}$" /></p>
		<p>

		<p>
		<label for="Description">Description:</label> 
			<textarea id="Description" name="Description" rows="10" cols="50" placeholder=" " /></textarea>
		</p>	
		
		<p>
			<label for="ClosingDate">Closing Date:</label>
			<input type="text" name="ClosingDate" value="<?php echo date('d/m/y');?>"/>
			<!--<input type="text" name= "Date Of Birth" id="ClosingDate" placeholder="dd/mm/yyyy" pattern="\d{1,2}\/\d{1,2}\/\d{4}"  required="required"/>
		-->
		</p>	
		
		<p>Position	
			<input id="Full-Time" value= "Full-Time" name="Position" type="radio" />
			<label for="Full-Time">Full-Time</label>
			<input id="Part-Time" value= "Part-Time" name="Position" type="radio"/>
			<label for="Part-Time">Part-Time</label>
		</p>
		<p>Contract	
			<input id="On-Going" value= "On-Going" name="Contract" type="radio" />
			<label for="On-Going">On-Going</label>
			<input id="Fixed Term" value= "Fixed Term" name="Contract" type="radio"/>
			<label for="Fixed Term">Fixed Term</label>
		</p>		
			
			<p>Application By:
			 
				<input type="checkbox" id="post" name="Application[]" value="Post" checked="checked"/>
				<label for="post">Post</label>			
	 
				<input type="checkbox" id="mail" name="Application[]" value="E-mail"/>
			<label for="2dgraphics">E-Mail</label>		
			</p>
	

		<p><label for="Location">Location:</label> 
			<select name="Location" id="Location">
			<option disabled selected value> ---- </option>
				<option value="VIC">VIC</option>			
				<option value="NSW">NSW</option>
				<option value="QLD">QLD</option>
				<option value="NT">NT</option>
				<option value="WA">WA</option>
				<option value="SA">SA</option>
				<option value="TAS">TAS</option>	
				<option value="ACT">ACT</option>					
		</select>
		</p>		
	<input type= "submit" name= "submit" value="Post"/>
	<input type= "reset" value="Reset Form"/>
	<p>*All fields are required. </p>
	</form>		
	
 <p><a href = "index.php">Return to Home Page</a></p>
	
 </body>
 </html>